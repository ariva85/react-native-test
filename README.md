react native app created with expo (https://facebook.github.io/react-native/docs/getting-started)

-install expo cli - "npm install -g expo-cli"
-navigate to project folder and install external dependencies - "npm install"
- run project - "expo start"

Features
Simple react app with basic navigation, Redux as store manager (with redux-thunk for async calls), react-native-extended-stylesheet for style variables and extra options.